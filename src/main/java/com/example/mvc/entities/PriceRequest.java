package com.example.mvc.entities;

import lombok.Data;

@Data
public class PriceRequest {
    private Booking booking;
    private Double extraPayment;
}
