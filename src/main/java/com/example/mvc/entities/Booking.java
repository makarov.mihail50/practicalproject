package com.example.mvc.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Booking {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @NotNull(message = "Date is mandatory")
    private Date dateOfBooking;

    @ManyToOne(fetch = FetchType.LAZY)
    private Customer client;

    @OneToOne
    private Car Car;

    @NotNull(message = "Date is mandatory")
    private Date dateFrom;

    @NotNull(message = "Date is mandatory")
    private Date dateTo;

    //possible values: active,cancelled
    @NotNull
    private String status;

    @ManyToMany
    @JoinTable(
            name = "booking_rental",
            joinColumns = @JoinColumn(name = "booking_id"),
            inverseJoinColumns = @JoinColumn(name = "rental_id")
    )
    private List<Branch> rentalBranch;

    @ManyToMany
    @JoinTable(
            name = "booking_return",
            joinColumns = @JoinColumn(name = "booking_id"),
            inverseJoinColumns = @JoinColumn(name = "return_id")
    )
    private List<Branch> returnBranch;

    @OneToOne
    private Revenue revenue;

}
