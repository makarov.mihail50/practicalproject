package com.example.mvc.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
public class Car {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Model type is mandatory")
    private String model;

    @NotBlank(message = "Body type is mandatory")
    private String bodyType;

    @NotNull(message = "Year of production is mandatory")
    private Integer yearOfProduction;

    @NotBlank(message = "Color is mandatory")
    private String color;

    @NotNull(message = "Mileage is mandatory")
    private Integer mileage;

    @NotBlank(message = "Status is mandatory")
    private String status;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "booking_id",referencedColumnName = "id")
    private Booking booking;

    @ManyToOne
    @JoinColumn(name = "branch_id")
    private Branch branch;


}
