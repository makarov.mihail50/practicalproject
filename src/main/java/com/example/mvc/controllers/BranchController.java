package com.example.mvc.controllers;

import com.example.mvc.entities.Branch;
import com.example.mvc.entities.Car;
import com.example.mvc.entities.Employee;
import com.example.mvc.services.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {

    private final BranchService branchService;

    @PostMapping("/save")
    public ResponseEntity<Branch> saveBranch(@Valid @RequestBody Branch branch){
        return ResponseEntity.ok(branchService.save(branch));
    }

    @GetMapping("/show-cars/{id}")
    public ResponseEntity<List<Car>> showAvailableCars(@PathVariable Long id){
        return ResponseEntity.ok(branchService.showAvailableCars(id));
    }

    @PostMapping("/add-cars")
    public ResponseEntity<Optional<Branch>> addCarsToBranch(@Valid @RequestBody List<Car> cars, Long id){
        Optional<Branch> b = branchService.setCarsToBranch(id, cars);
        return ResponseEntity.ok(b);
    }

    @GetMapping("/show-employees/{id}")
    public ResponseEntity<List<Employee>> showEmployees(@PathVariable Long id){
        return ResponseEntity.ok(branchService.showEmployees(id));
    }

    @PostMapping("/add-employee")
    public ResponseEntity<Optional<Branch>> addEmployeesToBranch(@Valid @RequestBody List<Employee> employees, Long id){
        Optional<Branch> b = branchService.setEmployeesToBranch(id, employees);
        return ResponseEntity.ok(b);
    }

}
