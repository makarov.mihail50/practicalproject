package com.example.mvc.controllers;

import com.example.mvc.entities.Car;
import com.example.mvc.entities.ID;
import com.example.mvc.entities.Message;
import com.example.mvc.services.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/car")
public class CarController {

    private final CarService carService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Car>> fetchAllCars() {
        return ResponseEntity.ok(carService.findAll());
    }

    @PostMapping("/save")
    public ResponseEntity<Car> carAdd(@Valid @RequestBody Car car) {
        return ResponseEntity.ok(carService.save(car));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Message> carDelete(@PathVariable("id") Long id){
        carService.deleteById(id);
        return ResponseEntity.ok(new Message("Record deleted"));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Message> carDelete2(@RequestBody ID id){
        carService.deleteById(id.getId());
        return ResponseEntity.ok(new Message("Record deleted"));
    }


}
