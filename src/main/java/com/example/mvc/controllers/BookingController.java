package com.example.mvc.controllers;

import com.example.mvc.entities.Booking;
import com.example.mvc.entities.Message;
import com.example.mvc.services.BookingService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/booking")
@RequiredArgsConstructor
public class BookingController {

    private final BookingService bookingService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Booking>> showAll() {
        return ResponseEntity.ok(bookingService.findAll());
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<Booking> showById(@PathVariable Long id) {
        return ResponseEntity.ok(bookingService.findById(id));
    }

    @PostMapping("/save")
    public ResponseEntity<Booking> addBooking(@Valid @RequestBody Booking booking) {
        return ResponseEntity.ok(bookingService.save(booking));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Message> deleteBooking(@PathVariable("id") Long id) {
        bookingService.deleteById(id);
        return ResponseEntity.ok(new Message("Record deleted"));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Message> deleteBooking(@Valid @RequestBody Booking booking) {
        bookingService.delete(booking);
        return ResponseEntity.ok(new Message("Record deleted"));
    }
}
