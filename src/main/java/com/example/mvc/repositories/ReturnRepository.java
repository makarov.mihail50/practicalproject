package com.example.mvc.repositories;

import com.example.mvc.entities.Return;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReturnRepository extends CrudRepository<Return, Long> {

    List<Return> findAll();

    Optional<Return> findById(Long id);

}
