package com.example.mvc.services;

import com.example.mvc.entities.ID;
import com.example.mvc.entities.Rent;
import com.example.mvc.repositories.CarRentalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CarRentalService {

    @Autowired
    private CarRentalRepository carRentalRepository;

    public List<Rent> findAll() {
        return carRentalRepository.findAll();
    }

    public Rent save(Rent id) {
        Rent cars = carRentalRepository.save(id);
        return cars;
    }

    public void deleteById(ID id) {
        carRentalRepository.deleteById(id.getId());
    }

    public void delete(Rent rent) {
        carRentalRepository.delete(rent);
    }
}
