package com.example.mvc.services;

import com.example.mvc.entities.Branch;
import com.example.mvc.entities.Car;
import com.example.mvc.entities.Employee;
import com.example.mvc.repositories.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BranchService {

    @Autowired
    private BranchRepository branchRepository;

    public List<Branch> findAll() {
        return branchRepository.findAll();
    }

    public Branch findById(Long id) {
        return branchRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid branch Id:" + id));
    }

    public Branch save(Branch id) {
        return branchRepository.save(id);
    }

    public List<Car> showAvailableCars(Long id) {
        Optional<Branch> b = branchRepository.findById(id);
        return b.get().getAvailableCars();
    }

    public List<Employee> showEmployees(Long id) {
        Optional<Branch> b = branchRepository.findById(id);
        return b.get().getEmployeeList();
    }

    public void deleteById(Long id) {
        branchRepository.deleteById(id);
    }

    public Optional<Branch> setCarsToBranch(Long branch_id, List<Car> cars) {
        Optional<Branch> b = branchRepository.findById(branch_id);
        b.get().setAvailableCars(cars);
        return b;
    }

    public Optional<Branch> setEmployeesToBranch(Long id, List<Employee> employees){
        Optional<Branch> b = branchRepository.findById(id);
        b.get().setEmployeeList(employees);
        return b;
    }
}
