package com.example.mvc.services;

import com.example.mvc.entities.Customer;
import com.example.mvc.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> findAll() {
        List<Customer> customers = customerRepository.findAll();
        return customers;
    }

    public Customer findById(Long id) {
        Customer customers = customerRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid customer Id:" + id));
        return customers;
    }

    public Customer save(Customer id) {
        Customer customers = customerRepository.save(id);
        return customers;
    }

    public void delete(Customer id) {
        customerRepository.delete(id);
    }

    public void deleteById(Long id) {
        customerRepository.deleteById(id);
    }


}
